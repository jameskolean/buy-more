# Switch to GraphQL Playground

update .evn
GATSBY_GRAPHQL_IDE=playground

# Contentful schema

see ./contentfulSchema/\*.json
